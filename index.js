const Client = require('./src/index');
const config = require('./config');
const {
	log
} = require('./utils/index');
const yiff = require('yiff');

yiff.sheri.setToken(config.api.sheri);
const {
	util
} = require('discord.js');

util.fetchRecommendedShards(config.token).then(async (count) => {

	new Client(config, count);
});