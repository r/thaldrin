const express = require('express');
const {
	hostname,
	port,
	authorization,
	source,
	invite,
	type
} = require('../config');
const db = require('quick.db');
const Backend = new db.table('backend');
const chalk = require('chalk');
const helmet = require('helmet');
const compression = require('compression');
const cors = require('cors');
const morgan = require('morgan');
var hbs = require('express-handlebars');

const app = express();

app.set('view engine', 'hbs');
app.engine(
	'hbs',
	hbs({
		extname: 'hbs',
		defaultView: 'default'
	})
);
app.set('json spaces', 4);
app.use('/assets', express.static(__dirname + '/assets'));
app.set('view options', {
	layout: false
});

app.use(express.json());
app.use(
	express.urlencoded({
		extended: true
	})
);
app.use(helmet());
app.use(compression());
app.use(cors());
// Logging
app.use(
	morgan((tokens, req, res) => {
		return [
			chalk.hex('#34ace0').bold(tokens.method(req, res)),
			chalk.hex('#ffb142').bold(tokens.status(req, res)),
			chalk.hex('#ff5252').bold(req.hostname + tokens.url(req, res)),
			chalk.hex('#2ed573').bold(tokens['response-time'](req, res) + 'ms'),
			chalk.hex('#f78fb3').bold('@ ' + tokens.date(req, res))
		].join(' ');
	})
);

let support = 'https://discord.gg/' + Backend.get('Info.invite');
app.use('/api', require('./routes/api'));
module.exports = (client) => {
	clientInfo: client,
	app.get('/', async (req, res) => {
		res.status(200).render('index', {
			layout: 'main',
			title: 'Thaldrin',
			subtitle: 'A Random Image and Utility Bot',
			bot: {
				users: client.users.size,
				guilds: client.guilds.size,
				channels: client.channels.size,
				commands: client.commands.size,
				invite: `https://${req.hostname}${type.beta ? ':8080' : ''}/invite`,
				source: `https://${req.hostname}${type.beta ? ':8080' : ''}/source`,
				support: `https://${req.hostname}${type.beta ? ':8080' : ''}/discord`
			}
		});
	});

	app.get('/discord', async (req, res) => {
		res.redirect(support);
	});
	app.get('/source', async (req, res) => {
		res.redirect(source);
	});
	app.get('/invite', async (req, res) => {
		res.redirect(invite);
	});
	app.get('/widgets', async (req, res) => {
		res.status(200).render('widgets', {
			layout: 'widgets',
			title: 'Widgets',
			subtitle: 'Thaldrin'
		});
	});
	/* app.get('/this', async (req, res) => {
		res.json(client);
	}); */

	/*  app.post('/vote', async (req, res) => {
        const body = req.body;
        const auth = req.header('Authorization');
    
        if (auth != authorization) return res.status(403), console.warn(`Vote rejected with authorization '${auth}'`);
        // if (body.bot != client.user.id) return res.status(403), console.warn(`Vote rejected with ID '${body.bot}'`);

        if (body.type == 'test') {
            console.log(`Test succeeded, is weekend:`, body.isWeekend);
        } else {
            console.log(`Vote`)
            client.vote(body.user);
        }
    }); */

	app.listen(port, () => {
		setTimeout(() => {
			console.log(`Listening on ${port}`);
		}, 1000 * 3);
	});
};