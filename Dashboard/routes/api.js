var express = require('express');
var api = express.Router();
var config = require('../../config');
var vars = require('../../vars');
var utils = require('../../utils');
const os = require('os');
let Bot = require('../server')

api.get('/', (req, res) => {
	res.status(200).jsonp({
		res: 'uwu'
	});
});

api.get('/ints/:kind/:user1?/:user2?', (req, res) => {
	if (utils.int[req.params.kind] === undefined) return res.status(404).jsonp({
		error: `This does not exist`
	});
	if (!req.params.user1 || !req.params.user2) return res.status(400).jsonp({
		error: `I need two users to work`
	});
	let LineFromUtils = utils.int[req.params.kind][parseInt(Math.random() * utils.int[req.params.kind].length)];
	let Line = LineFromUtils.replace(/0/g, utils.format.bold(req.params.user1)).replace(
		/1/g,
		utils.format.bold(req.params.user2)
	);
	let respo = LineFromUtils.replace(/0/g, req.params.user1).replace(/1/g, req.params.user2);
	res.status(200).jsonp({
		Line: respo,
		MarkdownLine: Line
	});
});

api.get(['/SourceFynnder', '/sourcefynnder', '/sauce'], async (req, res) => {
	let REPLY;
	let URLS = req.headers.images || req.query.images.split(',')
	await utils.SourceFynnder.APIFind(URLS).then(r => {
		console.log(r)
		res.status(200).jsonp({
			success: true,
			sources: r
		})
	}).catch(err => {
		res.status(400).jsonp({
			success: false,
			error: err.message
		})
	})

})


api.get('/system', (req, res) => {
	res.jsonp({
		name: vars.name,
		version: vars.version,
		hostname: os.hostname(),
		uptime: `${utils.format.uptime(process.uptime())}`
	});
});

module.exports = api;