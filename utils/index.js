const {
	Permissions: {
		FLAGS
	}
} = require('discord.js');
module.exports = {
	topic: require('./src/TopicSettings'),
	format: require('./src/format'),
	status: require('./src/statuses'),
	calc: require('./src/calc'),
	ShortLinks: require('./src/shortlinks'),
	SourceFynnder: require('./src/sourceFynnder'),
	emotes: require('./src/emotes'),
	db: require('./src/database'),
	log: require('./src/logs'),
	int: require('./src/interactions'),
	eco: require('./src/economy'),
	discord: require('discord.js'),
	yiff: require('yiff'),
	perms: FLAGS
};