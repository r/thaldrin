const {
	table
} = require("quick.db");
const ms = require("parse-ms");
const random = require("random-all");

const Users = new table("users");
const Backend = new table("backend");
let d;
let hasVoted = false;
let Placeholder = "318044130796109825";

module.exports = {
	CalculateFromMessage: async function (Message) {
		let hasVoted = await Users.get(`${Message.author.id}.voted`);
		content = Message.content;
		length = Message.content.length;
		if (hasVoted) d = 1
		else d = 0.01;
		e = (length * d).toFixed(1);
		return await Users.add(`${Message.author.id}.money.bank`, e);
	},
	Dailies: async function (User = Placeholder) {
		let timeout = 86400000;
		let amount = Math.floor(Math.random() * (200 - 150) + 150);

		let daily = await Users.get(`${User}.money.daily`);
		if (daily !== null && timeout - (Date.now() - daily) > 0) {
			let time = ms(timeout - (Date.now() - daily));

			throw new Error(`${time.hours}h ${time.minutes}m ${time.seconds}s`);
		} else {
			Users.add(`${User}.money.bank`, amount);
			Users.set(`${User}.money.daily`, Date.now());

			return {
				success: true,
				amount
			};
		}
	},
	weekly: async function (User = Placeholder) {
		let timeout = 604800000;
		let amount = Math.floor(Math.random() * (500 - 300) + 300);

		let weekly = await Users.get(`${User}.money.weekly`);
		if (weekly !== null && timeout - (Date.now() - weekly) > 0) {
			let time = ms(timeout - (Date.now() - weekly));
			throw new Error(
				`${time.days}d ${time.hours}h ${time.minutes}m ${time.seconds}s`
			);
		} else {
			Users.add(`${User}.money.bank`, amount);
			Users.set(`${User}.money.weekly`, Date.now());

			return {
				success: true,
				amount
			};
		}
	},
	// Math.floor(Math.random() * (500 - 300) + 300);
	quick: async function (kind, User = Placeholder) {
		let way = {
			work: {
				payout: Math.floor(Math.random() * (500 - 300) + 300),
				fine: Math.floor(Math.random() * (300 - 50) + 50),
				probability: random.getBoolean(85)
			},
			slut: {
				payout: Math.floor(Math.random() * (500 - 300) + 300),
				fine: Math.floor(Math.random() * (300 - 50) + 50),
				probability: random.getBoolean(60)
			},
			crime: {
				payout: Math.floor(Math.random() * (500 - 300) + 300),
				fine: Math.floor(Math.random() * (300 - 50) + 50),
				probability: random.getBoolean(55)
			}
		};
		if (way.hasOwnProperty(kind)) {
			if (way[kind].probability) {
				Users.add(`${User}.money.cash`, way[kind].payout);
				return {
					money: way[kind].payout,
					text: `You got ${way[kind].payout}<:coin:574116462260912138>  from doing ${kind}`
				}
			} else {
				Users.subtract(`${User}.money.cash`, way[kind].fine);
				return {
					money: way[kind].fine,
					text: `You were fined **${way[kind].fine}**<:coin:574116462260912138> `
				}

			}
		}
	}
};