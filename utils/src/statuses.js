function randomStatus(client) {
  const States = [{
      activity: {
        name: 'server fans whirr 💨',
        type: 'LISTENING'
      },
      status: 'online'
    },
    {
      activity: {
        name: 'True Damage - GIANTS',
        type: 'LISTENING'
      },
      status: 'online'
    },
    {
      activity: {
        name: `${client.guilds.size} Servers -w-`,
        type: 'WATCHING'
      },
      status: 'online'
    },
    {
      activity: {
        name: 'Δ & ♫',
        type: 'LISTENING'
      },
      status: 'online'
    },
    {
      activity: {
        name: `'help | thaldr.in`,
        type: 'WATCHING'
      },
      status: 'online'
    },
    {
      activity: {
        name: `Linux Servers`,
        type: 'WATCHING'
      },
      status: 'online'
    },
    {
      activity: {
        name: `e621 Videos`,
        type: 'WATCHING'
      },
      status: 'online'
    },
    {
      activity: {
        name: `WannaCry`,
        type: 'WATCHING'
      },
      status: 'online'
    },
    {
      activity: {
        name: `the Blockchain!`,
        type: 'WATCHING'
      },
      status: 'online'
    },
    {
      activity: {
        name: `something... ;3`,
        type: 'WATCHING'
      },
      status: 'online'
    },
    {
      activity: {
        name: `Skynet`,
        type: 'WATCHING'
      },
      status: 'online'
    },
    {
      activity: {
        name: `Half Life`,
        type: 'PLAYING'
      },
      status: 'online'
    },
    {
      activity: {
        name: `Half Life 2`,
        type: 'PLAYING'
      },
      status: 'online'
    },
    {
      activity: {
        name: `Portal`,
        type: 'PLAYING'
      },
      status: 'online'
    },
    {
      activity: {
        name: `Portal 2`,
        type: 'PLAYING'
      },
      status: 'online'
    }
    //	{ game: { name: `over ${thal.users.get('318044130796109825').username}~`, type: 'WATCHING' }, status: 'dnd' }
  ];
  let status = States[~~(Math.random() * States.length)];
  return client.user.setPresence(status);
}



module.exports = {
  playing: "PLAYING",
  watching: "WATCHING",
  listening: "LISTENING",
  streaming: "STREAMING",
  randomStatus
};