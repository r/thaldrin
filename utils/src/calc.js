module.exports = {
  userDays: function (user) {
    if (typeof user !== "object") throw new TypeError("Not an Object");

    let creation = new Date().getTime() - user.createdAt.getTime();

    let days = creation / 1000 / 60 / 60 / 24;

    return Math.round(days);
  },
  serverDays: function (server) {
    if (typeof server !== "object") throw new TypeError("Not an Object");

    let creation = new Date().getTime() - server.createdAt.getTime();

    let days = creation / 1000 / 60 / 60 / 24;

    return Math.round(days);
  }
};