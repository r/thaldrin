const db = require("quick.db");
const Servers = new db.table("servers");
const Users = new db.table("users");
//const { prefixes } = require('../../config');
const chalk = require("chalk");

const DatabaseDefaults = {
	user: {
		id: null,
		blacklist: {
			state: false,
			replied: false,
			reason: undefined
		}
	},
	server: {
		prefix: [],
		SourceFynnder: false,
		Shortlinks: false,
		embeds: true,
		rp_text: true,
		default_yiff: "gay"
	}
};

module.exports = {
	defaults: DatabaseDefaults,
	setupServer: async function (ctx) {
		ctx.utils.log.servers.setup(ctx.guild);
		if (Servers.get(ctx.guild.id) === null) {
			await Servers.set(ctx.guild.id, DatabaseDefaults.server);
		}
		ctx.utils.log.servers.fin(ctx.guild);
		return true;
	},
	prefix: {
		add: async function (ctx, Prefix) {
			let Prefixes = [];
			let New;
			Prefixes = await Servers.get(ctx.guild.id).prefix;

			if (Prefixes.includes(Prefix))
				throw new Error("Prefix already in Database");
			else {
				New = await Servers.push(`${ctx.guild.id}.prefix`, Prefix);
			}
			return New;
		},
		remove: async function (ctx, Prefix) {
			if (DatabaseDefaults.server.prefix.includes(Prefix)) {
				throw new Error("You cannot remove the Default Prefix(es)");
			}
			let Prefixes = ctx.db.servers.get(ctx.guild.id).prefix;

			if (!Prefixes.includes(Prefix)) {
				throw new Error("Prefix does not exist in Database");
			}

			let index = Prefixes.indexOf(Prefix);
			Prefixes.splice(index, 1);
			await Servers.set(`${ctx.guild.id}.prefix`, Prefixes);
		}
	},
	toggle: {
		SourceFynnder: async function (ctx) {
			let Server = await Servers.get(ctx.guild.id);
			Server.SourceFynnder ?
				console.log(
					`${chalk.green("✔")} Set ${chalk.blue(
              "SourceFynnder"
            )} in ${chalk.magenta(ctx.guild.id)} to ${chalk.red("false")} `
				) :
				console.log(
					`${chalk.green("✔")} Set ${chalk.blue(
              "SourceFynnder"
            )} in ${chalk.magenta(ctx.guild.id)} to ${chalk.green("true")} `
				);

			Server.SourceFynnder ?
				await Servers.set(`${ctx.guild.id}.SourceFynnder`, false) :
				await Servers.set(`${ctx.guild.id}.SourceFynnder`, true);
		},
		Shortlinks: async function (ctx) {
			let Server = await Servers.get(ctx.guild.id);
			Server.Shortlinks ?
				console.log(
					`${chalk.green("✔")} Set ${chalk.blue(
              "Shortlinks"
            )} in ${chalk.magenta(ctx.guild.id)} to ${chalk.red("false")} `
				) :
				console.log(
					`${chalk.green("✔")} Set ${chalk.blue(
              "Shortlinks"
            )} in ${chalk.magenta(ctx.guild.id)} to ${chalk.green("true")} `
				);

			Server.Shortlinks ?
				await Servers.set(`${ctx.guild.id}.Shortlinks`, false) :
				await Servers.set(`${ctx.guild.id}.Shortlinks`, true);
		},
		Embeds: async function (ctx) {
			let Server = await Servers.get(ctx.guild.id);
			Server.embeds ?
				console.log(
					`${chalk.green("✔")} Set ${chalk.blue("Emdeds")} in ${chalk.magenta(
              ctx.guild.id
            )} to ${chalk.red("false")} `
				) :
				console.log(
					`${chalk.green("✔")} Set ${chalk.blue("Embeds")} in ${chalk.magenta(
              ctx.guild.id
            )} to ${chalk.green("true")} `
				);

			Server.embeds ?
				await Servers.set(`${ctx.guild.id}.embeds`, false) :
				await Servers.set(`${ctx.guild.id}.embeds`, true);
		},
		Text: async function (ctx) {
			let Server = await Servers.get(ctx.guild.id);
			Server.rp_text ?
				console.log(
					`${chalk.green("✔")} Set ${chalk.blue(
              "RP Text"
            )} in ${chalk.magenta(ctx.guild.id)} to ${chalk.red("false")} `
				) :
				console.log(
					`${chalk.green("✔")} Set ${chalk.blue(
              "RP Text"
            )} in ${chalk.magenta(ctx.guild.id)} to ${chalk.green("true")} `
				);

			Server.rp_text ?
				await Servers.set(`${ctx.guild.id}.rp_text`, false) :
				await Servers.set(`${ctx.guild.id}.rp_text`, true);
		}
	},
	blacklist: async function (user, kind, reason) {
		switch (kind) {
			case "add":
			case "a":
				let Y = await Users.set(user, {
					id: user,
					blacklist: {
						state: true,
						replied: false,
						reason: reason
					}
				});
				return Y;
				break;
			case "remove":
			case "r":
				let Z = await Users.set(user, {
					id: user,
					blacklist: {
						state: false,
						replied: false,
						reason: undefined
					}
				});
				return Z;
				break;
			case "check":
			case "status":
			case "c":
			case "s":
				//let X = await Users.get(user);
				//console.log(X)
				//if (X === null) X = DatabaseDefaults.user
				//if(X.blacklist === null)
				let W = await Users.get(user)
				let X = DatabaseDefaults.user.blacklist

				if (W === null) K = X
				else if (W.blacklist === undefined || W.blacklist === null) K = X
				else K = W.blacklist

				//console.log(K)
				return K
				break;

			default:
				break;
		}
	}
};