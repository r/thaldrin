const ora = require("ora");
const vars = require("../../vars");
const chalk = require("chalk");
const BotSpinner = new ora({
  discardStdin: false
});
const shardSpinner = new ora({
  discardStdin: false
});
const ServerSpinner = new ora({
  discardStdin: false
});

module.exports = async = {
  starting: async function () {
    BotSpinner.text = `${vars.name} - v${vars.version} is starting`;
    BotSpinner.spinner = "moon";
    BotSpinner.start();
    return BotSpinner;
  },
  stopSpinner: function () {
    return BotSpinner.stop();
  },
  hasStarted: async function () {
    BotSpinner.succeed(`${vars.name} - v${vars.version} has started`);
    return;
  },
  shardReady: async function (text) {
    shardSpinner.text = text;
    shardSpinner.spinner = "moon";
    return shardSpinner.start();
  },
  shardSpinnerStarted: function () {
    return shardSpinner.succeed();
  },
  servers: {
    setup: async function (server) {
      ServerSpinner.text = `Setting up ${chalk.red(server.name)} | ${chalk.red(
        server.id
      )}...`;
      ServerSpinner.start();
    },
    fin: async function (server) {
      ServerSpinner.succeed(
        `${chalk.red(server.name)} | ${chalk.red(server.id)} has been set up.`
      );
    }
  }
};