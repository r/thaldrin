const TopicSettings = /thaldrin.(no-cmd|no-SL|no-SF)/g;
let PlaceholderTopic = `
Commands for staff members

thaldrin.no-cmd
thaldrin.no-SL`

//if (PlaceholderTopic.match(TopicSettings)) return console.log(PlaceholderTopic.match(TopicSettings))

module.exports = {
    includesSetting: function (setting, topic) {
        if (!setting || !topic) return;

        let TopicSetting = topic.match(TopicSettings)
        let Bool;
        if (TopicSetting) {
            if (TopicSetting.includes(setting)) {
                Bool = true
            }
        } else {
            Bool = false
        }
        return Bool
    }
}