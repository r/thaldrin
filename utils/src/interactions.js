const hug = [
	'0 hugs 1',
	'1 gets a tight hug from 0',
	'1 is getting a hug from 0',
	'Is 0 hugging 1? They sure are!',
	'0 hugs 1 in a lovable way',
	'0 hugs 1 with a very warm smile',
	'0 hugs 1 in an attempt to cheer them up',
	'0 sneaks up behind 1 and gives him/her a surprise hug!',
	'0 lunges at 1, wrapping his/her arms around lovingly!',
	"1 wasn't suspecting anything at first, but then suddenly 0 appears and hugs him/her!",
	'0 blushes a little bit, carefully approaching 1 and giving him/her a tender hug.',
	'0 suddenly yells out loud; "Tactical hug incoming!".',
	'0 lifts 1 from the ground and hugs them tightly.',

	'0 and 1 run towards eachother, arms open, not unlike the classic grassy-field-reunion scene. They meet in the middle with a strong embrace.',
	'0 scoots closer over to 1 on a park bench, then wraps his/her arms around the unsuspecting hug victim.',
	'0 confidently steps forward, giving 1 a tight hug.',
	"0 gives 1 a big ol' hug.",
	'1 is suddenly hugged by 0!'
];
const kiss = [
	'0 kisses 1',
	'0 gives 1 a kiss',
	'1 gets a surprise kiss from 0',
	'0 appears to be kissing 1. Lets leave them to it :3'
];
const boop = [ '0 boops 1', "0 boops 1's Snoot", "1's Snoot got booped by 0", '0 nuzzles 1 and boops their Snoot' ];

const cuddle = [ '0 cuddles 1', '0 cuddles 1 lovingly', '0 has 1 in their lap and cuddles them lovingly' ];

const lick = [
	'0 licked 1 to the point where he/she murrred/purred',
	'0 licked 1 in the name of love. Murr!',
	'0 was feeling a little playful and decided to lick 1',
	'0 slides over to 1 and licks them',
	'0 nibbles and licks 1 playfully',
	'0 slides over to 1 and licks and cuddles 1'
];

const hold = [ '0 holds 1', '0 holds 1 tight' ];

const bulge = [ "0 eyes 1's  bulge", "0 likes to see 1's bulge~" ];

const suck = [ "0 sucks 1's cock with hungry eyes~", '1 lets 0 suck their cock~' ];

const bang = [ '0 bangs 1', '1 moans as 0 bangs them', '1 moans loudly as 0 bangs them hard~' ];

module.exports = {
	hug,
	kiss,
	boop,
	cuddle,
	lick,
	hold,
	bulge,
	suck,
	bang
};
