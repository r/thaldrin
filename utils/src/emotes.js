module.exports = {
	status: {
		online: '<:oxONLINE:480145455708110850> Online',
		streaming: '<:oxSTREAMING:487933554894962688> Streaming',
		idle: '<:oxIDLE:480145455183953921> Idle',
		dnd: '<:oxDND:480145455104262165> Do not Disturb',
		offline: '<:oxOFFLINE:480145455330754571> Offline'
	},
	MemberStatus: {
		online: '<:oxONLINE:480145455708110850>: ',
		streaming: '<:oxSTREAMING:487933554894962688>: ',
		idle: '<:oxIDLE:480145455183953921>: ',
		dnd: '<:oxDND:480145455104262165>: ',
		offline: '<:oxOFFLINE:480145455330754571>: '
	},

	social: {
		twitter: '<:twitter:517392526622064640>',
		patreon: '<:patreon:465243291877900300>',
		github: '<:github:487283925417459732>',
		trello: '<:trello:526452412244951041>',
		discord: '<:discord:517392526647361557>'
	},

	settings: {
		cog: '<:settings:350172481157464064>',
		on: '<:thalYes:444843380187332608>',
		off: '<:thalNo:447651589902041102> ',
		locked: '<:locked:350172480318603265>',
		unlocked: '<:unlocked:350172481664974848>',
		save: '<:save:350172480821919745>',
		trash: '<:trash:350172481794998273>'
	},

	eval: {
		working: '<:success:350172481186955267>',
		warning: '<:warning:350172481757118478>',
		error: '<:error:350172479936921611>'
	},

	perms: {
		auth: '<:authorization:350172478019993614>'
	},
	features: {
		commerce: '<:StoreChannel:640333912442404864> Store',
		news: '<:NewsChannel:640333912417239050> News',
		public: ':globe_with_meridians: Public',
		invite_splash: ':frame_photo: Invite Splash',
		animated_icon: 'Animated Icon',
		banner: 'Banner',
		vanity_url: ':link: Vanity URL'
	},
	regions: {
		'us-east': ':flag_us: US East',
		'us-south': ':flag_us: US South',
		'us-central': ':flag_us: US Central',
		singapore: ':flag_sg: Singapore',
		sydney: ':flag_au: Sydney',
		southafrika: 'South Afrika',
		kongkong: ':flag_hk: Hong Kong',
		brazil: ':flag_br: Brazil',
		amsterdam: ':flag_nl: Amsterdam',
		japan: ':flag_jp: Japan',
		london: ':flag_gb: London',
		india: ':flag_in: India',
		europe: ':flag_eu: Europe',
		'eu-west': ':flag_eu: Europe West',
		'eu-central': ':flag_eu: Europe Central',
		russia: ':flag_ru: Russia',
		frankfurt: ':flag_de: Frankfurt',
		dubai: 'Dubai'
	},
	random: {
		pin: '<:pin:350172480725581838>',
		loading: '<a:loading:645308535390994432>'
	}
};
