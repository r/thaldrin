const axios = require('axios');
const db = require('quick.db');
const backend = new db.table('backend');
const Servers = new db.table('servers');
let md5 = new RegExp(
	'((?:!)?https?://static[0-9]*.(?:e621|e926).net/data/(?:sample/|preview/|)[0-9a-f]{2}/[0-9a-f]{2}/([0-9a-f]{32}).([0-9a-z]+))',
	'igm'
);

let search_md5 = 'https://e621.net/post/show.json?md5=';
let e6 = 'https://e621.net/post/show/';
let e9 = 'https://e926.net/post/show/';

const version = '0.1.0';

async function SourceFynnderBot(enabled, msg) {
	if (!enabled || enabled === null || enabled === undefined) return;
	res = msg.content.match(md5);
	if (!res) return;

	let Sources = [];
	for (const m in res) {
		let URL = res[m];
		let hash = URL.split(md5)[2];

		let {
			data
		} = await axios.get(search_md5 + hash, {
			headers: {
				'user-agent': `SourceFynnder/${version} (ry / codepupper)`
			}
		});
		if (data.rating === 's') {
			Source = e9 + data.id;
		} else {
			Source = e6 + data.id;
		}
		Sources.push(`:link::mag: ${Source}`);
	}
	msg.channel.send(Sources);
	await backend.add('SourceFynnder.found', Sources.length);
	await backend.add('SourceFynnder.foundBot', Sources.length);
	await Servers.add(`${msg.guild.id}.foundSources`, Sources.length);
};

async function SourceFynnderAPI(url) {
	url = url.toString().replace(/\,/g, ' ')
	res = url.match(md5);
	if (!res) throw new Error('Not a Valid e621/e926 URL');

	let Sources = [];
	for (const m in res) {
		let URL = res[m];
		let hash = URL.split(md5)[2];

		let {
			data
		} = await axios.get(search_md5 + hash, {
			headers: {
				'user-agent': `SourceFynnder/${version} (ry / codepupper)`
			}
		});
		if (data.rating === 's') {
			Source = e9 + data.id;
		} else {
			Source = e6 + data.id;
		}
		Sources.push(`${Source}`);
	}
	await backend.add('SourceFynnder.found', Sources.length);
	await backend.add('SourceFynnder.foundAPI', Sources.length);
	return Sources
};

module.exports = {
	SourceFynnder: SourceFynnderBot,
	APIFind: SourceFynnderAPI
}