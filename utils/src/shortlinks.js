const ShortLinkReg = /(?:\s|^)(gh|gl|gd|owo|aud|sg|ttv|teknik|bb|yt|bc|bcu|sc|bot|sw|tw|npm|xkcd)\/([a-zA-Z0-9-_.#/!]*)/g;
const ShortLinks = {
	gh: 'https://github.com/$link$',
	gl: 'https://gitlab.com/$link$',
	yt: 'https://youtu.be/$link$',
	tw: 'https://twitter.com/$link$',
	npm: 'https://npm.im/$link$',
	ttv: 'https://twitch.tv/$link$',
	gd: 'https://gitdab.com/$link$',
	owo: 'https://owo.codes/$link$',
	sg: 'https://git.supernets.org/$link$',
	teknik: 'https://git.teknik.io/$link$',
	bb: 'https://bitbucket.org/$link$',
	bc: 'https://$link$.bandcamp.com/',
	bcu: 'https://bandcamp.com/$link$',
	sc: 'https://soundcloud.com/$link$',
	aud: 'https://audius.co/$link$',
	//	aur: 'https://aur.archlinux.org/packages/$link$',
	sw: 'https://steamcommunity.com/sharedfiles/filedetails/?id=$link$',
	bot: '<https://discordapp.com/oauth2/authorize?client_id=$link$&scope=bot>',
	xkcd: 'https://xkcd.com/$link$'
};
const ShortLinkDirs = {
	gh: 'Github',
	gl: 'Gitlab',
	gd: 'Gitdab',
	yt: 'Youtube',
	tw: 'Twitter',
	npm: 'NPM',
	ttv: 'TwitchTv',
	owo: 'owo_codes',
	sg: 'Supernets_Git',
	aud: 'Audius',
	teknik: 'Teknik_Git',
	bb: 'BitBucket',
	bc: 'Bandcamp_Band',
	bcu: 'Bandcamp_User',
	sc: 'Soundcloud',
	//	aur: 'Arch_Packages',
	sw: 'Steam_Workshop',
	bot: 'Discord_Bot_Invite',
	xkcd: 'xkcd'
};
const db = require('quick.db');
const backend = new db.table('backend');
module.exports = async function Shortlink(enabled, msg) {
	if (!enabled || enabled === null || enabled === undefined) return;
	let res = msg.content.match(ShortLinkReg);
	if (!res) return;
	res = res.map((x) => (x.startsWith(' ') ? x.substring(1) : x));
	let links = [];
	let Amount = [];

	for (const m in res) {
		for (const x in ShortLinks) {
			let url = res[m];
			if (!url.startsWith(x)) continue;
			url = url.replace(x + '/', '');

			if (x == 'gh' || x == 'gl' || x == 'gd') {
				url = url.replace('#', '/issues/');
			}

			if (x == 'gl') {
				url = url.replace('!', '/merge_requests/');
			} else if (x == 'gd') {
				url = url.replace('!', '/pulls/');
			} else if (x == 'gh') {
				url = url.replace('!', '/pull/');
			}

			url = ShortLinks[x].replace('$link$', url);
			links.push(`${url}`);
			await backend.add(`Shortlink.${ShortLinkDirs[x]}`, 1);
			await backend.add(`Shortlink.total`, 1);
		}
	}

	msg.channel.send(links.join('\n'));
};