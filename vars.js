module.exports = {
	name: 'Thaldrin',
	version: '3.6.4',
	color: '#ff995d',
	hostname: '127.2.11.1',
	//hostname: 'localhost',
	port: '8080',
	type: {
		beta: true,
		prod: false
	},
	prefixes: ["'", `<@434662676547764244> `, '<@!434662676547764244> '],
	developers: [{
		id: '318044130796109825',
		nick: 'ry'
	}],
	logs: {
		usage: '663740167684620318'
	},
	contributors: [{
			id: '150745989836308480',
			nick: 'Cyn',
			reason: 'Shortlink Code :3'
		},
		{
			id: '356323373443448843',
			nick: 'Twiggy',
			reason: 'Letting me use his Sona as the Mascot <3'
		},
		{
			id: '289947794142396427',
			nick: 'Fynn',
			reason: 'Letting me use his name for `SourceFynnder` <3'
		}
	],
	source: 'https://gitdab.com/r/thaldrin',
	invite: 'https://discordapp.com/oauth2/authorize?client_id=434662676547764244&scope=bot&permissions=379968'
};