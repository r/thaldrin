const Command = require('../../src/structures/Command');
const yiff = require('yiff');
const { MessageEmbed } = require('discord.js');

module.exports = class Bang extends Command {
	constructor() {
		super({
			name: 'bang',
			description: 'Bang a user',
			aliases: [ 'fuck' ],
			module: 'Roleplay',
			cooldown: 2,
			guildOnly: true,
			developerOnly: false,
			nsfw: true
		});
	}

	async command(ctx) {
		if (ctx.msg.mentions.members.size === 0) return ctx.send('please mention a user ;w;');
		if (ctx.msg.mentions.members.first().id === ctx.client.user.id)
			return ctx.send("Don't bang me! Bang someone else!");
		if (ctx.msg.mentions.members.first().id === ctx.author.id)
			return ctx.send(`Don't you want to bang someone other than yourself?`);

		const Server = await ctx.db.servers.get(ctx.guild.id);
		let Line;
		let Settings;
		if (Server === null) {
			Settings = ctx.utils.db.defaults.server;
		} else {
			Settings = Server;
		}
		if (Settings.rp_text) {
			const LineFromUtils = ctx.utils.int.bang[parseInt(Math.random() * ctx.utils.int.bang.length)];
			Line = LineFromUtils.replace(/0/g, ctx.utils.format.bold(ctx.author.username)).replace(
				/1/g,
				ctx.utils.format.bold(ctx.msg.mentions.members.first().user.username)
			);
		} else {
			Line = undefined;
		}

		let req;
		let Message;
		await yiff.furrybot.nsfw.bang().then((E) => (req = E));

		if (Settings.embeds) {
			Message = new MessageEmbed()
				.setColor(ctx.config.color)
				.setImage(req)
				.setFooter(`${ctx.client.user.username} - Provided by furry.bot`, ctx.client.user.avatarURL());
			if (Line) {
				Message.setDescription(Line);
			}
		} else {
			if (Line) {
				Message = `${Line}\n${req}`;
			} else {
				Message = `${req}`;
			}
		}
		ctx.send(Message);
	}
};
