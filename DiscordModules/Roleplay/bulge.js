const Command = require('../../src/structures/Command');
const yiff = require('yiff');
const { MessageEmbed } = require('discord.js');

module.exports = class Bulge extends Command {
	constructor() {
		super({
			name: 'bulge',
			description: `Look at a User's Bulge`,
			aliases: [],
			module: 'Roleplay',
			cooldown: 2,
			guildOnly: false,
			developerOnly: false,
			nsfw: true
		});
	}

	async command(ctx) {
		let Line;
		const Server = await ctx.db.servers.get(ctx.guild.id);
		let Settings;
		if (Server === null) {
			Settings = ctx.utils.db.defaults.server;
		} else {
			Settings = Server;
		}
		if (ctx.msg.mentions.members.size > 0 && Settings.rp_text) {
			const LineFromUtils = ctx.utils.int.bulge[parseInt(Math.random() * ctx.utils.int.bulge.length)];
			Line = LineFromUtils.replace(/0/g, ctx.utils.format.bold(ctx.author.username)).replace(
				/1/g,
				ctx.utils.format.bold(ctx.msg.mentions.members.first().user.username)
			);
		} else {
			Line = undefined;
		}

		let req;
		let Message;
		await yiff.furrybot.nsfw.bulge().then((E) => (req = E));

		if (Settings.embeds) {
			Message = new MessageEmbed()
				.setColor(ctx.config.color)
				.setImage(req)
				.setFooter(`${ctx.client.user.username} - Provided by furry.bot`, ctx.client.user.avatarURL());
			if (Line) {
				Message.setDescription(Line);
			}
		} else {
			if (Line) {
				Message = `${Line}\n${req}`;
			} else {
				Message = `${req}`;
			}
		}
		ctx.send(Message);
	}
};
