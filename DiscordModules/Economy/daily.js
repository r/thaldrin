const Command = require('../../src/structures/Command');
module.exports = class Daily extends Command {
    constructor() {
        super({
            name: 'daily',
            description: 'Get your Dailies',
            aliases: ['d'],
            module: 'Economy',
            cooldown: 5,
            guildOnly: true,
            developerOnly: true,
            nsfw: false
        });
    }


    async command(ctx) {
        ctx.utils.eco.Dailies().then(r => {
            ctx.send(`**${r.amount}**<:coin:574116462260912138>   has been added to your bank.`)
        }).catch(error => ctx.send(`You can get your dailies in ${error.message}`))
    }
}