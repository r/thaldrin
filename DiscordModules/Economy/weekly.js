const Command = require('../../src/structures/Command');
module.exports = class Weekly extends Command {
    constructor() {
        super({
            name: 'weekly',
            description: 'Get your Dailies',
            aliases: ['w'],
            module: 'Economy',
            cooldown: 5,
            guildOnly: true,
            developerOnly: true,
            nsfw: false
        });
    }


    async command(ctx) {
        ctx.utils.eco.weekly().then(r => {
            ctx.send(`**${r.amount}**<:coin:574116462260912138>  has been added to your bank.`)
        }).catch(error => ctx.send(`You can get your weeklies in ${error.message}`))
    }
}