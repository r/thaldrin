const Command = require('../../src/structures/Command');
module.exports = class Crime extends Command {
    constructor() {
        super({
            name: 'crime',
            description: 'Commit crimes',
            aliases: [],
            module: 'Economy',
            cooldown: 15,
            guildOnly: true,
            developerOnly: true,
            nsfw: false
        });
    }


    async command(ctx) {
        ctx.utils.eco.quick('crime', ctx.author.id).then(r => {
            ctx.send(r.text)
        }).catch(error => ctx.send(`Error ${error.message}`))
    }
}