const Command = require('../../src/structures/Command');
module.exports = class Work extends Command {
    constructor() {
        super({
            name: 'work',
            description: 'Work for your money',
            aliases: [],
            module: 'Economy',
            cooldown: 15,
            guildOnly: true,
            developerOnly: true,
            nsfw: false
        });
    }


    async command(ctx) {
        ctx.utils.eco.quick('work', ctx.author.id).then(r => {
            ctx.send(r.text)
        }).catch(error => ctx.send(`Error ${error.message}`))
    }
}