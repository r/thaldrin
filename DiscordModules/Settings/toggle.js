const Command = require('../../src/structures/Command');
module.exports = class Toggle extends Command {
	constructor() {
		super({
			name: 'toggle',
			description: 'Toggle various Settings',
			aliases: [ 't' ],
			module: 'Settings',
			cooldown: 5,
			guildOnly: true,
			developerOnly: false,
			AuthorPermissions: [ 'MANAGE_GUILD' ]
		});
	}

	async command(ctx) {
		const Embed = new ctx.utils.discord.MessageEmbed().setColor(ctx.config.color);
		let ARG = ctx.args[0];
		switch (ARG) {
			case 'source':
			case 'sauce':
			case 'sf':
			case 'sourcefynnder':
				await ctx.utils.db.toggle.SourceFynnder(ctx);
				Embed.setTitle(`Changed SourceFynnder Setting`).setDescription(
					`Now set to ${ctx.db.servers.get(ctx.guild.id).SourceFynnder
						? ctx.utils.emotes.settings.on
						: ctx.utils.emotes.settings.off}`
				);
				ctx.send(Embed);
				break;
			case 'shortlinks':
			case 'shortlink':
			case 'sl':
				await ctx.utils.db.toggle.Shortlinks(ctx);
				Embed.setTitle(`Changed Shortlink Setting`).setDescription(
					`Now set to ${ctx.db.servers.get(ctx.guild.id).Shortlinks
						? ctx.utils.emotes.settings.on
						: ctx.utils.emotes.settings.off}`
				);
				ctx.send(Embed);

				break;
			case 'embed':
			case 'embeds':
				await ctx.utils.db.toggle.Embeds(ctx);
				Embed.setTitle(`Changed Image Embed Setting`).setDescription(
					`Now set to ${ctx.db.servers.get(ctx.guild.id).embeds
						? ctx.utils.emotes.settings.on
						: ctx.utils.emotes.settings.off}`
				);
				ctx.send(Embed);
				break;
			case 'text':
			case 'rp_text':
				await ctx.utils.db.toggle.Text(ctx);
				Embed.setTitle(`Changed Image Text Setting`).setDescription(
					`Now set to ${ctx.db.servers.get(ctx.guild.id).rp_text
						? ctx.utils.emotes.settings.on
						: ctx.utils.emotes.settings.off}`
				);
				ctx.send(Embed);
				break;
			default:
				Embed.setTitle('Help')
					.setDescription(`All settings have their own Toggles, see the list below to know which one's which`)
					.addField('SourceFynnder', 'sf sauce source sourcefynnder', true)
					.addField('Shortlinks', 'sl shortlink shortlinks', true)
					.addField('Image Embeds', 'embed embeds', true)
					.addField('Image Text', 'text rp_text', true);
				ctx.send(Embed);
				break;
		}
	}
};
