const Command = require('../../src/structures/Command');
module.exports = class Prefix extends Command {
	constructor() {
		super({
			name: 'prefix',
			description: 'Add or Remove a prefix',
			aliases: [],
			module: 'Settings',
			cooldown: 10,
			guildOnly: true,
			developerOnly: false,
			AuthorPermissions: [ 'MANAGE_GUILD' ]
		});
	}

	async command(ctx) {
		let ARG = ctx.args[0];
		ctx.args.shift();
		let Prefix = ctx.args.join(' ');
		let Server = await ctx.db.servers.get(ctx.guild.id);
		Server.prefix.shift();
		Server.prefix.shift();
		Server.prefix.shift();
		switch (ARG) {
			case 'a':
			case 'add':
				if (ctx.args === [] || ctx.args.join(' ').trim() === '') return ctx.send('No Prefix was given');

				ctx.utils.db.prefix
					.add(ctx, Prefix)
					.then(async () => {
						let aPrefixEmbed = new ctx.utils.discord.MessageEmbed();
						aPrefixEmbed.setColor(ctx.config.color);
						let NServer = await ctx.db.servers.get(ctx.guild.id);
						aPrefixEmbed.setTitle(`Prefixes for ${ctx.guild.name}`);
						aPrefixEmbed.setDescription(`${NServer.prefix.join(' | ') || ctx.config.prefixes}`);
						ctx.send(aPrefixEmbed);
					})
					.catch((err) => ctx.send(err));
				break;
			case 'r':
			case 'remove':
				if (ctx.args === [] || ctx.args.join(' ').trim() === '') return ctx.send('No Prefix was given');
				ctx.utils.db.prefix
					.remove(ctx, Prefix)
					.then(async () => {
						let NServer = await ctx.db.servers.get(ctx.guild.id);
						let rPrefixEmbed = new ctx.utils.discord.MessageEmbed();
						rPrefixEmbed.setColor(ctx.config.color);
						rPrefixEmbed.setTitle(`Prefixes for ${ctx.guild.name}`);
						if (NServer.prefix === []) {
							rPrefixEmbed.setDescription(ctx.config.prefixes.join(' **|**'));
						} else {
							rPrefixEmbed.setDescription(
								`${NServer.prefix.join(' | ') || ctx.config.prefixes.join(' **|** ')}`
							);
						}
						ctx.send(rPrefixEmbed);
					})
					.catch((err) => console.log(err));
				break;
			default:
			let dPrefixEmbed = new ctx.utils.discord.MessageEmbed();
				dPrefixEmbed.setTitle('Help');
				dPrefixEmbed.addField('a / add <prefix>', 'Add a Prefix', true);
				dPrefixEmbed.addField('r / remove <prefix>', 'Remove a Prefix', true);
				ctx.send(dPrefixEmbed);
				break;
		}
	}
};
