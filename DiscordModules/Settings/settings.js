const Command = require('../../src/structures/Command');

function Toggle(bool, ctx) {
	return bool ? `${ctx.utils.emotes.settings.on}*_ _*` : `${ctx.utils.emotes.settings.off}*_ _*`
}

module.exports = class Settings extends Command {
	constructor() {
		super({
			name: 'settings',
			description: 'Show the Settings of this Server',
			aliases: ['config'],
			module: 'Settings',
			cooldown: 5,
			guildOnly: true,
			developerOnly: false
		});
	}


	async command(ctx) {
		const SettingsEmbed = new ctx.utils.discord.MessageEmbed();
		SettingsEmbed.setColor(ctx.config.color);
		const Server = await ctx.db.servers.get(ctx.guild.id);
		// console.log(Server);
		if (Server !== null) {
			SettingsEmbed.setTitle(`Settings for ${ctx.guild.name}`)
				.addField('Prefixes', Server.prefix.join(', ') || `<@${ctx.client.user.id}> or \`'\``, false)
				.addField(
					'SourceFynnder',
					Toggle(Server.SourceFynnder, ctx), true
				)
				.addField(
					'Shortlinks',
					Toggle(Server.Shortlinks, ctx), true)
				.addBlankField(true)
				.addField(
					'Image Embeds',
					Toggle(Server.embeds, ctx), true
				)
				.addField(
					'Image Text',
					Toggle(Server.rp_text, ctx), true
				);
			//	.addField('Default Yiff', Server.default_yiff, true);
			ctx.send(SettingsEmbed);
		} else {
			SettingsEmbed.setTitle(`No Settings for ${ctx.guild.name}`).setDescription(
				`You shouldn't see this.\n Your Server might not have been set up Properly when you invited me.\n\nPlease [join my support server](https://discord.gg/xNAcF8m) and notify my Developer`
			);

			ctx.send(SettingsEmbed);
		}
	}
};