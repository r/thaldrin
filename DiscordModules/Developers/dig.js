const Command = require("../../src/structures/Command");
const exec = require("shell-exec");
const dig = require("node-dig-dns");
const {
  MessageEmbed
} = require("discord.js");
let DomainReg = new RegExp(
  `(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]`
);
module.exports = class Dig extends Command {
  constructor() {
    super({
      name: "dig",
      description: "dig website dns information stuff",
      aliases: [],
      module: "Developers",
      cooldown: 10,
      guildOnly: false,
      developerOnly: true,
      nsfw: false
    });
  }

  async command(ctx) {
    let count = 0;
    let domain = ctx.args[0];
    let type = ctx.args[1];
    const DIG = new MessageEmbed().setTitle(`${domain} (${type})`);
    if (domain.match(DomainReg)) {
      try {
        let result = await dig([domain, type]);

        result.answer.forEach(r => {
          count++;
          DIG.addField(`Answer ${count}`, r.value);
        });
      } catch (error) {
        DIG.setDescription(
          `Either the Domain you are trying to dig for doesn't exist or the record you are requesting does not exist.`
        );
      }
    }
    ctx.send(DIG);
  }
};