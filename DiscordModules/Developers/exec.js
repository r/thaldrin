const Command = require('../../src/structures/Command');
const exec = require('shell-exec');
module.exports = class Exec extends Command {
	constructor() {
		super({
			name: 'exec',
			description: 'Execute shell commands',
			aliases: [ 'ex' ],
			module: 'Developers',
			cooldown: 1,
			guildOnly: false,
			developerOnly: true,
			nsfw: false
		});
	}

	async command(ctx) {
		const trying = await ctx.send('Attempting to execute ' + ctx.utils.format.bold(ctx.args.join(' ')));

		await exec(ctx.args.join(' '))
			.then((r) => {
				trying.edit('```bash\n' + r.stdout + '```');
			})
			.catch((error) => {
				trying.edit('Failed to execute ' + ctx.utils.format.bold(ctx.args.join(' ')));
			});
	}
};
