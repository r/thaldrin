const Command = require('../../src/structures/Command');
const exec = require('shell-exec');
module.exports = class Update extends Command {
	constructor() {
		super({
			name: 'update',
			description: 'Execute shell commands',
			aliases: [ 'up', 'pull' ],
			module: 'Developers',
			cooldown: 1,
			guildOnly: false,
			developerOnly: true,
			nsfw: false
		});
	}

	async command(ctx) {
		const trying = await ctx.send(
			`${ctx.utils.emotes.random.loading} Getting the latest updates from ${ctx.config.source.replace(
				'https://',
				''
			)} ${ctx.utils.emotes.random.loading}`
		);

		await exec('git pull')
			.then((r) => {
				trying.edit('```fix\n' + r.stdout + '```');
			})
			.catch((error) => {
				trying.edit(`Failed to get the latest updates.`);
				console.error(error);
			});
	}
};
