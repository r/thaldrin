const Command = require('../../src/structures/Command');

module.exports = class Reload extends Command {
	constructor() {
		super({
			name: 'reload',
			description: 'Reload a command without restarting the process.',
			aliases: [ 're' ],
			module: 'Developers',
			cooldown: 0,
			guildOnly: false,
			developerOnly: true
		});
	}

	async command(ctx) {
		if (!ctx.args.length) return;
		const date = Date.now();

		const data = ctx.args[0];
		const [ module, command ] = data.split('/');

		if (!module || !command) return;

		try {
			delete require.cache[require.resolve(`../${module}/${command}`)];
			delete ctx.client.commands.get(command);

			const cmd = require(`../${module}/${command}`);
			const Command = new cmd();
			ctx.client.commands.set(Command.name, Command);

			return ctx.send(`Reloaded \`${Command.name}\` in ${(Date.now() - date) / 1000}s.`);
		} catch (err) {
			return ctx.send(`Failed to reload the command.\n\`${err}\``);
		}
	}
};
