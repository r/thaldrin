const Command = require('../../src/structures/Command');
module.exports = class Blacklist extends Command {
    constructor() {
        super({
            name: 'blacklist',
            description: 'Master the Blacklist',
            aliases: ['bl'],
            module: 'Developers',
            cooldown: 0,
            guildOnly: false,
            developerOnly: true
        });
    }

    async command(ctx) {
        console.log(ctx.args)
        let ACTION = ctx.args[0]
        let ID = ctx.args[1]
        let REASON = ctx.args.shift().shift().join(' ')
        //  let REASON = ctx.args[2]

        let X = await ctx.utils.db.blacklist(ID, ACTION, REASON)
        console.log(X)
    }
};