const Command = require('../../src/structures/Command');
module.exports = class Stop extends Command {
	constructor() {
		super({
			name: 'stop',
			description: 'Stops the bot',
			aliases: [],
			module: 'Developers',
			cooldown: 0,
			guildOnly: false,
			developerOnly: true
		});
	}

	async command(ctx) {
		await ctx.send('Restarting.');
		process.exit();
	}
};
