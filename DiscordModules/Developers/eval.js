const Command = require('../../src/structures/Command');
const { table } = require('quick.db');
const Servers = new table('servers');
const Users = new table('users');
const Bot = new table('bot');

const clean = (text) => {
	if (typeof text == 'string')
		return text.replace(/`/g, '`' + String.fromCharCode(8203)).replace(/@/g, '@' + String.fromCharCode(8203));
	else return text;
};

module.exports = class Eval extends Command {
	constructor() {
		super({
			name: 'eval',
			description: 'Run JavaScript code directly from the process.',
			aliases: [ 'ev', 'e' ],
			module: 'Developers',
			cooldown: 0,
			guildOnly: false,
			developerOnly: true
		});
	}

	async command(ctx) {
		if (!ctx.args.length) return;

		const client = ctx.client;

		let code = ctx.args.join(' ');
		let silent = false;

		if (code.endsWith('-s')) (code = code.split('-s')[0]), (silent = true);
		if (code.endsWith('--silent')) (code = code.split('--silent')[0]), (silent = true);

		try {
			let evaled = await eval(code);

			if (typeof evaled != 'string')
				evaled = require('util').inspect(evaled, false, await ctx.db.backend.get('eval'));

			evaled.replace(new RegExp(client.token.replace(/\./g, '\\.', 'g')), 'uwu');

			if (!silent) {
				ctx
					.send(`\`\`\`js\n${clean(evaled)}\n\`\`\``)
					.then(async (m) => {
						await m.react('📥');
						await m.react('🗑');
					})
					.catch((err) => {
						ctx
							.send(`\`Content is over 2,000 characters: react to upload to Hastebin\``)
							.then(async (m) => {
								client.lastEval = clean(evaled);

								await m.react('📥');
								await m.react('🗑');
							});
					});
			}
		} catch (error) {
			ctx
				.send(`\`\`\`js\n${clean(error)}\n\`\`\``)
				.then(async (m) => {
					await m.react('📥');
					await m.react('🗑');
				})
				.catch((err) => {
					ctx.send(`\`Content is over 2,000 characters: react to upload to Hastebin\``).then(async (m) => {
						client.lastEval = clean(error);

						await m.react('📥');
						await m.react('🗑');
					});
				});
		}
	}
};
