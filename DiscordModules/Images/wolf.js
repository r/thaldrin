const Command = require('../../src/structures/Command');
const yiff = require('yiff');
const { MessageEmbed } = require('discord.js');

module.exports = class Wolf extends Command {
	constructor() {
		super({
			name: 'wolf',
			description: 'Get a random wolf',
			aliases: [ 'woof', 'wolves', 'awoo' ],
			module: 'Images',
			cooldown: 2,
			guildOnly: false,
			developerOnly: false,
			nsfw: false
		});
	}

	async command(ctx) {
		const Server = await ctx.db.servers.get(ctx.guild.id);
		let Settings;
		if (Server === null) {
			Settings = ctx.utils.db.defaults.server;
		} else {
			Settings = Server;
		}

		let req;
		let Message;
		await yiff.sheri.animals.wolf().then((E) => (req = E));
		if (Settings.embeds) {
			Message = new MessageEmbed()
				.setImage(req.url)
				.setColor(ctx.config.color)
				.setFooter(`${ctx.client.user.username} - Provided by sheri.bot`, ctx.client.user.avatarURL());
		} else {
			Message = `${req}`;
		}
		ctx.send(Message);
	}
};
