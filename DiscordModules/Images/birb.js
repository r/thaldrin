const Command = require('../../src/structures/Command');
const yiff = require('yiff');
const { MessageEmbed } = require('discord.js');

module.exports = class Birb extends Command {
	constructor() {
		super({
			name: 'bird',
			description: 'Get a random birb',
			aliases: [ 'birb' ],
			module: 'Images',
			cooldown: 2,
			guildOnly: false,
			developerOnly: false,
			nsfw: false
		});
	}

	async command(ctx) {
		const Server = await ctx.db.servers.get(ctx.guild.id);
		let Settings;
		if (Server === null) {
			Settings = ctx.utils.db.defaults.server;
		} else {
			Settings = Server;
		}

		let req;
		let Message;
		await yiff.shibe.birds().then((E) => (req = E));
		if (Settings.embeds) {
			Message = new MessageEmbed()
				.setColor(ctx.config.color)
				.setImage(req)
				.setFooter(`${ctx.client.user.username} - Provided by shibe.online`, ctx.client.user.avatarURL());
		} else {
			Message = `${req}`;
		}
		ctx.send(Message);
	}
};
