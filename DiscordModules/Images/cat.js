const Command = require('../../src/structures/Command');
const yiff = require('yiff');
const { MessageEmbed } = require('discord.js');

module.exports = class Cat extends Command {
	constructor() {
		super({
			name: 'cat',
			description: 'Get a random cat',
			aliases: [ 'meow' ],
			module: 'Images',
			cooldown: 2,
			guildOnly: false,
			developerOnly: false,
			nsfw: false
		});
	}

	async command(ctx) {
		const Server = await ctx.db.servers.get(ctx.guild.id);
		let Settings;
		if (Server === null) {
			Settings = ctx.utils.db.defaults.server;
		} else {
			Settings = Server;
		}

		let req;
		let Message;
		await yiff.shibe.cats().then((E) => (req = E));
		if (Settings.embeds) {
			Message = new MessageEmbed()
				.setColor(ctx.config.color)
				.setImage(req)
				.setFooter(`${ctx.client.user.username} - Provided by shibe.online`, ctx.client.user.avatarURL());
		} else {
			Message = `${req}`;
		}
		ctx.send(Message);
	}
};
