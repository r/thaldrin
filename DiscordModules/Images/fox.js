const Command = require('../../src/structures/Command');
const yiff = require('yiff');
const { MessageEmbed } = require('discord.js');

module.exports = class Fox extends Command {
	constructor() {
		super({
			name: 'fox',
			description: 'Get a random fox',
			aliases: [ 'yip' ],
			module: 'Images',
			cooldown: 2,
			guildOnly: false,
			developerOnly: false,
			nsfw: false
		});
	}

	async command(ctx) {
		const Server = await ctx.db.servers.get(ctx.guild.id);
		let Settings;
		if (Server === null) {
			Settings = ctx.utils.db.defaults.server;
		} else {
			Settings = Server;
		}

		let req;
		let Message;
		await yiff.sheri.animals.fox().then((E) => (req = E));
		if (Settings.embeds) {
			Message = new MessageEmbed()
				.setColor(ctx.config.color)
				.setImage(req.url)
				.setFooter(`${ctx.client.user.username} - Provided by sheri.bot`, ctx.client.user.avatarURL());
		} else {
			Message = `${req}`;
		}
		ctx.send(Message);
	}
};
