const Command = require('../../src/structures/Command');
const yiff = require('yiff');
const { MessageEmbed } = require('discord.js');
let Icon = 'https://werewoof.tech/e621.png';

module.exports = class E621 extends Command {
	constructor() {
		super({
			name: 'e926',
			description: 'Get Images from e621',
			aliases: [ 'e9' ],
			module: 'Images',
			cooldown: 5,
			guildOnly: false,
			developerOnly: false,
			nsfw: false
		});
	}
	async command(ctx) {
		let Embed = new MessageEmbed().setColor('RED');
		if (ctx.args < 1) {
			Embed.setTitle('Search Terms').setDescription('I need more tags than that to search for an Image.');
			return ctx.send(Embed);
		}
		const Server = await ctx.db.servers.get(ctx.guild.id);
		let Settings;
		if (Server === null) {
			Settings = ctx.utils.db.defaults.server;
		} else {
			Settings = Server;
		}
		let req;
		let Message;
		await yiff.e926.request(ctx.args.join(' ')).then((E) => (req = E));
		if (Settings.embeds) {
			Message = new MessageEmbed()
				.setImage(req.image)
				.setColor(ctx.config.color)
				.setAuthor('e621.net', Icon, `https://e926.net/post/show/${req.postID}`)
				.setFooter(`${ctx.client.user.username} - e926.net`, ctx.client.user.avatarURL());
		} else {
			Message = `<https://e926.net/post/show/${req.postID}>\n${req.image}`;
		}
		ctx.send(Message);
	}
};
