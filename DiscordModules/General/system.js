const Command = require("../../src/structures/Command");
const { MessageEmbed, version: DiscordVersion } = require("discord.js");
const { developers, contributors, source, color } = require("../../config");
const os = require("os");
const db = require("quick.db");
const Backend = new db.table("backend");
function format(seconds) {
  function pad(s) {
    return (s < 10 ? "0" : "") + s;
  }
  var hours = Math.floor(seconds / (60 * 60));
  var minutes = Math.floor((seconds % (60 * 60)) / 60);
  var seconds = Math.floor(seconds % 60);

  return pad(hours) + "h " + pad(minutes) + "m " + pad(seconds) + "s";
}

module.exports = class Info extends Command {
  constructor() {
    super({
      name: "system",
      description: "Show System Info",
      aliases: ["sys", "sysinfo"],
      module: "General",
      cooldown: 0,
      guildOnly: false,
      developerOnly: false
    });
  }

  async command(ctx) {
    let SystemEmbed = new MessageEmbed()
      .setTitle(`${ctx.client.user.username} | v${ctx.vars.version}`)
      .setColor(color)
      .setDescription(
        `Made by ${ctx.utils.format.bold(
          ctx.client.users.find(user => user.id === "318044130796109825").tag
        )}`
      )
      .addField("Language", "Javascript", true)
      .addField("Library", `d.js - v${DiscordVersion}`, true)
      .addField("Uptime", `${format(process.uptime())}`, true)
      .addField("Node", `${process.version}`, true)
      .addField("Servers", ctx.client.guilds.size, true)
      .addField("Users", ctx.client.users.size, true)
      .addField(
        "Total sauce found",
        ctx.db.backend.get("SourceFynnder.found"),
        true
      );

    ctx.send(SystemEmbed);
  }
};
