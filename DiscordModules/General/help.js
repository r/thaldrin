const Command = require("../../src/structures/Command");
const {
	MessageEmbed
} = require("discord.js");

function CapFirstLetter(string) {
	if (typeof string == undefined) return;
	var firstLetter = string[0] || string.charAt(0);
	return firstLetter ? firstLetter.toUpperCase() + string.slice(1) : "";
}

module.exports = class nHelp extends Command {
	constructor() {
		super({
			name: "help",
			description: "View a list of available commands or information on a specific command.",
			aliases: ['h', '?'],
			module: "General",
			cooldown: 0,
			guildOnly: false,
			developerOnly: false
		});
	}

	async command(ctx) {
		let Help = new MessageEmbed();
		const commands = {
			General: {
				cs: ctx.client.commands
					.filter(command => command.module == "General")
					.map(c => c),
				name: "General"
			},
			Settings: {
				cs: ctx.client.commands
					.filter(command => command.module == "Settings")
					.map(c => c),
				name: "Settings"
			},
			Images: {
				cs: ctx.client.commands
					.filter(command => command.module == "Images")
					.map(c => c),
				name: "Images"
			},
			Roleplay: {
				cs: ctx.client.commands
					.filter(command => command.module == "Roleplay")
					.map(c => c),
				name: "Roleplay"
			}
		};
		let lengths = [];
		let names = ["General", "Settings", "Images", "Roleplay"];

		for (const i in commands) {
			if (commands.hasOwnProperty(i)) {
				const c = commands[i];
				lengths.push(c.cs.length);
			}
		}
		let start = 0;

		Help.setAuthor(ctx.author.tag, ctx.author.avatarURL())
			.setTitle("Command Help")
			.setTimestamp(new Date())
			.setColor(ctx.vars.color)
			.setFooter(`${ctx.client.user.username}`, ctx.client.user.avatarURL());

		if (ctx.args.length === 0) {
			lengths.forEach(c => {
				Help.addField(
					`${names[start]} (${c})`,
					`${ctx.utils.format.code(`'help --${names[start].toLowerCase()}`)}`,
					true
				);
				start++;
			});
			return ctx.send(Help);
		}

		let category, cmd;
		if (ctx.args.length > 0) {
			category = ctx.args[0].slice(2);
			cmd = ctx.args[1];
		}

		if (commands.hasOwnProperty(CapFirstLetter(category))) {
			let short = [];
			let long = [];
			commands[`${CapFirstLetter(category)}`].cs.forEach(c => {
				short.push(`${ctx.utils.format.code(c.name)} - ${c.description}`);
				long.push(c);
			});

			if (ctx.args.length === 2) {
				let c = long.filter(
					c =>
					c.name === cmd.toLowerCase() ||
					(c.aliases && c.aliases.includes(cmd.toLowerCase()))
				);
				let Text;
				if (c.length === 0) {
					Text = "There is no command with that name in this category.";
					Help.setDescription(Text);
					return ctx.send(Help);
				} else {
					let fields = [{
							name: "Module",
							value: c[0].module,
							inline: true
						},
						{
							name: "Aliases",
							value: c[0].aliases.length == 0 ?
								"No aliases" : c[0].aliases.join(", "),
							inline: true
						},
						{
							name: "Cooldown",
							value: c[0].cooldown == 0 ? "No cooldown" : `${c[0].cooldown}s`,
							inline: true
						},
						{
							name: "Server only?",
							value: c[0].guildOnly ? "Yes" : "No",
							inline: true
						},
						{
							name: "Permissions needed",
							value: c[0].AuthorPermissions ? c[0].AuthorPermissions : "None",
							inline: true
						},
						{
							name: "Developers only?",
							value: c[0].developerOnly ? "Yes" : "No",
							inline: true
						}
					];
					fields.forEach(i => {
						Help.addField(i.name, i.value, i.inline);
					});

					return ctx.send(Help);
				}
			} else {
				Help.setDescription(
					`Use \n${ctx.utils.format.code(
            `'help --<cagegory> <command or alias> to get help on a specific command`
          )}\n\n${short.join("\n")}`
				);
				return ctx.send(Help);
			}
		} else {
			Help.setDescription("This Category does not exist");
			return ctx.send(Help);
		}
	}
};