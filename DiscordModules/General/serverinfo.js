const Command = require('../../src/structures/Command');

module.exports = class ServerInfo extends Command {
	constructor() {
		super({
			name: 'serverinfo',
			description: 'Shows Information about your Server!',
			aliases: [ 'server', 'sinfo', 'si' ],
			module: 'General',
			cooldown: 0,
			guildOnly: true,
			developerOnly: false
		});
	}

	async command(ctx) {
		const total = ctx.guild.members.size;
		const users = ctx.guild.members.filter((m) => !m.user.bot).size;
		const bots = ctx.guild.members.filter((m) => m.user.bot).size;
		const Features = ctx.guild.features;
		let OnlineUsers =
			ctx.utils.emotes.MemberStatus['online'] +
			ctx.guild.members.filter((m) => m.presence.status === 'online').size;
		let DNDUsers =
			ctx.utils.emotes.MemberStatus['dnd'] + ctx.guild.members.filter((m) => m.presence.status === 'dnd').size;
		let IdleUsers =
			ctx.utils.emotes.MemberStatus['idle'] + ctx.guild.members.filter((m) => m.presence.status === 'idle').size;
		let OfflineUsers =
			ctx.utils.emotes.MemberStatus['offline'] +
			ctx.guild.members.filter((m) => m.presence.status === 'offline').size;

		let features = [];
		Features.forEach((f) => features.push(ctx.utils.emotes.features[f.toLowerCase()] || f));

		let Embed = new ctx.utils.discord.MessageEmbed();
		Embed.setTitle(ctx.guild.name)
			.setColor(ctx.config.color)
			.setThumbnail(ctx.guild.iconURL())
			.addField(
				'Members',
				`${ctx.utils.format.bold(`Total:`)} ${ctx.utils.format.code(total)}\n${ctx.utils.format.bold(
					`Users:`
				)} ${ctx.utils.format.code(users)}\n${ctx.utils.format.bold(`Bots:`)} ${ctx.utils.format.code(bots)}
				
				${OnlineUsers}
				${DNDUsers}
				${IdleUsers}
				${OfflineUsers}`
			)
			.addField('Owner', ctx.guild.owner, true)
			.addField('Large?', ctx.guild.large ? ctx.utils.emotes.settings.on : ctx.utils.emotes.settings.off, true)
			.addField(
				'Boost Level / Boosters',
				`${ctx.guild.premiumTier} / ${ctx.guild.premiumSubscriptionCount}`,
				true
			)
			.addField('Region', ctx.utils.emotes.regions[ctx.guild.region] || ctx.guild.region, true)
			.addField('Features', features.join(' **|** ') || 'None');
		if (ctx.guild.vanityURLCode) {
			Embed.addField(
				'Vanity URL',
				`[discord.gg/${ctx.guild.vanityURLCode}](https://discord.gg/${ctx.guild.vanityURLCode})`
			);
		}
		/* 			.addField('Large?')
			.addField('Large?'); */

		ctx.send(Embed);
	}
};
