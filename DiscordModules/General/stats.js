const Command = require('../../src/structures/Command');
const {
    MessageEmbed: Embed
} = require('discord.js')
module.exports = class Statistics extends Command {
    constructor() {
        super({
            name: 'statistics',
            description: 'Get in-depth bot Stats',
            aliases: ['stats'],
            module: 'General',
            cooldown: 5,
            guildOnly: false,
            developerOnly: false,
            nsfw: false
        });
    }


    async command(ctx) {
        let StatisticsEmbed = new Embed().setTitle(`${ctx.vars.name} v${ctx.vars.version} Statistics`).setColor(ctx.vars.color)
        var SortUsage = [];
        var SortSL = [];
        var SortSF = [];
        let usage = ctx.db.backend.get('usage')
        let Shortlink = ctx.db.backend.get('Shortlink')
        let SourceFynnder = ctx.db.backend.get('SourceFynnder')
        // change data value to the one you showed in pic
        for (var type in usage) {
            SortUsage.push([type, usage[type]]);
        }
        for (var type in Shortlink) {
            SortSL.push([type, Shortlink[type]]);
        }
        for (var type in SourceFynnder) {
            SortSF.push([type, SourceFynnder[type]]);
        }

        let usages = SortUsage.sort((a, b) => b[1] - a[1])
        let SLs = SortSL.sort((a, b) => b[1] - a[1])
        let SFs = SortSF.sort((a, b) => b[1] - a[1])

        let UsageStats = ''
        let SLStats = ''
        let SFStats = ''
        usages.forEach(use => {
            UsageStats += `${ctx.utils.format.bold(use[0])} - \`${use[1]}\`\n`
        })
        SLs.forEach(SL => {
            SLStats += `${ctx.utils.format.bold(SL[0])} - \`${SL[1]}\`\n`
        })
        SFs.forEach(SF => {
            SFStats += `${ctx.utils.format.bold(SF[0]).replace(/found/g,'Found w/')} - \`${SF[1]}\`\n`
        })
        StatisticsEmbed.addField("Uptime", `${ctx.utils.format.uptime(process.uptime())}`, true)
            .addField("Servers", ctx.client.guilds.size, true)
            .addField("Users", ctx.client.users.size, true)
            .addField('Command Usage', UsageStats, true)
            .addField('Shortlink Usage', SLStats, true)
            .addField('SourceFynnder Usage', SFStats.replace(/Found w\//, 'Total'), true)
        ctx.send(StatisticsEmbed)
    }
}