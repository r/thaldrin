const Command = require('../../src/structures/Command');
const D = require('discord.js');
module.exports = class Suggest extends Command {
	constructor() {
		super({
			name: 'suggest',
			description: 'Suggest something for the Bot',
			aliases: [ 'sug', 'suggestion' ],
			module: 'General',
			cooldown: 10,
			guildOnly: false,
			developerOnly: false,
			nsfw: false
		});
	}

	async command(ctx) {
		/* throw new Error('Testing'); */
		let Misuse = new D.MessageEmbed()
			.setTitle('Misuse of Command')
			.setDescription(
				`There was 
				Please make sure to make your Suggestion as Detailed as possible.\n\nAbuse of this command will lead to complete denial of command usage.`
			);
		if (ctx.args.length < 1) {
			return ctx.send(abuse);
		}

		let Suggestion = ctx.args.join(' ').split('|')
        if (Suggestion.length < 2) return ctx.send(`Only the Title doesn't help, please add Text after the \`|\` to send me your Suggestion`)
        if (Suggestion[1] === '') return ctx.send(`Only the Title doesn't help, please add Text after the \`|\` to send me your Suggestion`)



		ctx.trello
			.addCard(
				Suggestion[0],
				`
		${Suggestion[1]}

       Author: ${ctx.author.tag} (${ctx.author.id})
       Server: ${ctx.guild.name} (${ctx.guild.id})`,
				ctx.config.trello.boards.suggestions
			)
			.then((r) => {
				ctx.trello.addLabelToCard(r.id, ctx.config.trello.labels.suggestions).catch(console.error);
				let reply = new D.MessageEmbed()
					.setTitle('Suggestion Received')
					.setColor(ctx.config.color)
					.setDescription(ctx.args.join(' '))
					.addField('URL', r.url, true)
					.setThumbnail(ctx.client.user.avatarURL());

				ctx.send(reply);
			});
	}
};
