const Command = require('../../src/structures/Command');
const {
	MessageEmbed,
	version: DiscordVersion
} = require('discord.js');
const db = require('quick.db');
const Backend = new db.table('backend');
module.exports = class Info extends Command {
	constructor() {
		super({
			name: 'info',
			description: 'Show the Makers and Contributors of the Bot',
			aliases: ['about'],
			module: 'General',
			cooldown: 0,
			guildOnly: false,
			developerOnly: false
		});
	}

	async command(ctx) {
		let result;
		const {
			developers,
			contributors,
			source,
			color
		} = ctx.vars
		const contribs = [];
		for (const {
				id,
				nick,
				reason
			} of contributors) {
			const user = await ctx.client.users.fetch(id);
			contribs.push(`${user} (${nick}) - ${reason}`);
		}
		const Contributors = contribs.join('\n');
		let CreditEmbed = new MessageEmbed()
			.setTitle(`${ctx.client.user.username}, a Random Image and Utility Bot`)
			.setColor(color)
			.setDescription(
				`Made by ${ctx.utils.format.bold(
					ctx.client.users.find((user) => user.id === '318044130796109825').tag
				)}`
			)
			/* 			.addField('Language', 'Javascript', true)
			.addField('Library', `d.js - v${DiscordVersion}`, true)
			.addField('Node', `${process.version}`, true)
			.addField('Servers', ctx.client.guilds.size, true)
			.addField('Users', ctx.client.users.size, true) */
			.addField('Contributors', Contributors)
			.addField('Source', `[gd/r/thaldrin](${source})`, true)
			.addField(
				'Support Server',
				`[${ctx.client.guilds.get('438316852347666432').name}](https://discord.gg/${ctx.db.backend.get(
					'Info.invite'
				)})`,
				true
			)
			.addField('Website', `[thaldr.in](https://thaldr.in)`, true);

		ctx.send(CreditEmbed);
	}
};