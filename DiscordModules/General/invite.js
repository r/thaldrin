const Command = require('../../src/structures/Command');
const D = require('discord.js');
module.exports = class Invite extends Command {
	constructor() {
		super({
			name: 'invite',
			description: 'Get the Bot Invite.',
			module: 'General',
			cooldown: 0,
			guildOnly: false,
			developerOnly: false
		});
	}

	async command(ctx) {
		const Embed = new D.MessageEmbed()
			.setDescription(`**[Invite Me!](https://thaldr.in/invite)**`)
			.setColor(ctx.config.color)
			.setFooter(`${ctx.client.user.username}`, ctx.client.user.avatarURL());
		return ctx.send(Embed);
	}
};
