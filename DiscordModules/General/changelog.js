const Command = require('../../src/structures/Command');

module.exports = class Changelog extends Command {
    constructor() {
        super({
            name: 'changelog',
            description: 'View the latest changes of the bot',
            aliases: ['chl', 'c'],
            module: 'Developers',
            cooldown: 5,
            guildOnly: false,
            developerOnly: true,
            nsfw: false
        });
    }


    async command(ctx) {
        let isDev;
        if (ctx.vars.developers.find((dev) => ctx.author.id == dev.id)) isDev = true

        const ValidActions = /(set|view)/g

        let action = ctx.args[0]
        ctx.args.shift()

        if (!action.match(ValidActions)) return ctx.send(`You can only ${isDev ? ctx.utils.format.code('view/set'): ctx.utils.format.code('view')} a Changelog`)

        //       ctx.db.backend.get()

        let Changelog = ctx.args.join(' ').split('|')
        let version = Changelog[0].replace(/\./g, '_')
        let text = Changelog[1]
        //  console.log(action, version, text)

        if (action === 'set') {
            if (!isDev) return;
            await ctx.db.backend.set(version.trim(), text)
            ctx.send('Changelog set, posting...')
            ctx.client.channels.find(c => c.id == '572515873156235307').send(`**__Version ${version.replace(/\_/g, '.')}__**\n\n${text.trim()}`)
            return ctx.send('Sent.')
        } else if (action === 'view') {
            let Change = ctx.db.backend.get(version)
            if (Change === null) return ctx.send(`That is not a valid Version (or it's not in my Database).\nMy latest version is ${ctx.utils.format.bold(ctx.vars.version)}.`)
            ctx.send(`**__Version ${version.replace(/\_/g, '.')}__**\n\n${Change}`)
        }
    }
}
