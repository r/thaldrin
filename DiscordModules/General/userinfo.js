const Command = require('../../src/structures/Command');
module.exports = class Userinfo extends Command {
	constructor() {
		super({
			name: 'userinfo',
			description: 'Shows User Information',
			aliases: [ 'user', 'ui', 'uinfo' ],
			module: 'General',
			cooldown: 2,
			guildOnly: true,
			developerOnly: false,
			nsfw: false
		});
	}

	async command(ctx) {
		let User;
		let Game;
		let Embed = new ctx.utils.discord.MessageEmbed();
		User = ctx.msg.mentions.members.first() || /* ctx.args[0] || */ ctx.member;

		if (User.presence.activity === null) {
			Game = 'Playing Nothing';
		} else {
			if (User.presence.activity.name === 'Custom Status') {
				Game = `• ${User.presence.activity.state || 'Error'}`;
			} else {
				if (User.presence.activity.details === null) {
					User.presence.activity.details = '*_ _*';
					User.presence.activity.state = '*_ _*';
				} else {
					Game = `
					${User.presence.activity.name}
					${User.presence.activity.details}
					${User.presence.activity.state}`;
				}
			}
		}
		let Roles = [];
		let x = 0;
		for (const role of User.roles) {
			Roles.push(`<@&${role[0]}>`);
		}

		Embed.setTitle(`Info on ${User.nickname || User.user.username}`)
			.setColor(ctx.config.color)
			.addField('Username', User.user.tag, true)
			.addField('ID', User.user.id, true)
			.addField(`Status | ${ctx.utils.emotes.status[User.presence.status]}`, Game);
		if (Roles.length > 15) {
			Embed.addField(`Roles [${Roles.length}]`, 'Too many to list' || 'Error');
		} else {
			Embed.addField(`Roles [${Roles.length}]`, Roles.join(' **|** '));
		}
		Embed.setThumbnail(User.user.avatarURL());

		ctx.send(Embed).catch((err) => ctx.send('```js\n' + err + '```'));
	}
};
