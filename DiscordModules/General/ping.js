const Command = require('../../src/structures/Command');

module.exports = class Ping extends Command {
    constructor() {
        super({
            name: 'ping',
            description: 'Pings Discord to check the API and gateway latency.',
            aliases: [],
            module: 'General',
            cooldown: 0,
            guildOnly: false,
            developerOnly: false
        });
    }

    async command(ctx) {
        const m = await ctx.send(`Pinging..`);

        const rest = Math.round(m.createdTimestamp - ctx.msg.createdTimestamp);
        const ws = Math.round(ctx.client.ws.ping);
        const shard = Math.round(ctx.guild.shard.ping);

        return m.edit(`REST ${rest / 1000}s (${rest}ms)\nWS ${ws / 1000}s (${ws}ms)`);
    }
}

// \nShard Avg. ${shard / 1000}s (${shard}ms) [${ctx.guild.shard.pings.join(', ')}]