const Command = require('../../src/structures/Command');
const D = require('discord.js');
module.exports = class Bug extends Command {
	constructor() {
		super({
			name: 'bug',
			description: 'Report a Bug in the bot',
			aliases: [ 'report', 'bugreport' ],
			module: 'General',
			cooldown: 10,
			guildOnly: false,
			developerOnly: false,
			nsfw: false
		});
	}

	async command(ctx) {
		/* throw new Error('Testing'); */
		let abuse = new D.MessageEmbed()
			.setTitle('Misuse of Command')
			.setDescription(
				`Please make sure to make your Suggestion as Detailed as possible.\n\nAbuse of this command will lead to complete denial of command usage.`
			);
		if (ctx.args.length < 1) {
			return ctx.send(abuse);
		}

		ctx.trello
			.addCard(
				ctx.args.join(' '),
				`
       Author: ${ctx.author.tag} (${ctx.author.id})
       Server: ${ctx.guild.name} (${ctx.guild.id})`,
				ctx.config.trello.boards.bugs
			)
			.then((r) => {
				ctx.trello.addLabelToCard(r.id, ctx.config.trello.labels.bugs).catch(console.error);
				let reply = new D.MessageEmbed()
					.setTitle('Report Received')
					.setColor(ctx.config.color)
					.setDescription(ctx.args.join(' '))
					.addField('URL', r.url, true)
					.setThumbnail(ctx.client.user.avatarURL());

				ctx.send(reply);
			});
	}
};
