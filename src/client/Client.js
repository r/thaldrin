const {
	Client,
	Collection
} = require('discord.js');
const {
	readdirSync: read
} = require('fs');
const server = require('../../Dashboard/server');

module.exports = class Thaldrin extends Client {
	constructor(config, shards) {
		super({
			disableEveryone: true,
			disabledEvents: ['TYPING_START'],
			shardCount: shards,
			totalShardCount: shards
		});

		this.commands = new Collection();
		this.cooldowns = new Collection();
		// this.queues = new Collection();
		this.config = config;

		this.lastEval = null;

		this.login(config.token);
		this.load();
	}

	async load() {
		const events = await read('./DiscordEvents');
		const modules = await read('./DiscordModules');

		server(this);

		events.filter((f) => f.endsWith('.js')).forEach((file) => {
			try {
				const event = require(`../../DiscordEvents/${file}`);

				this.on(event.name, event.run.bind(null, this));
			} catch (err) {
				console.error(err);
			}
		});

		modules.filter((f) => !f.endsWith('.js')).forEach(async (module) => {
			const commands = await read(`./DiscordModules/${module}`);

			commands.filter((f) => f.endsWith('.js')).forEach((command) => {
				try {
					const file = require(`../../DiscordModules/${module}/${command}`);
					const Command = new file();

					this.commands.set(Command.name, Command);
				} catch (err) {
					console.error(err);
				}
			});
		});
	}
};