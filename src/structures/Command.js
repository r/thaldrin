module.exports = class Command {
	constructor(command) {
		this.name = command.name || '';
		this.description = command.description || '';
		this.aliases = command.aliases || [];
		this.module = command.module || '';
		this.cooldown = command.cooldown || 0;
		this.guildOnly = command.guildOnly || false;
		this.developerOnly = command.developerOnly || false;
		this.nsfw = command.nsfw || false;
		this.AuthorPermissions = command.AuthorPermissions || 'NONE';
	}
};
