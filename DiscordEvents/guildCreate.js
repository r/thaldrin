const { logChannel } = require('../config');
const { MessageEmbed } = require('discord.js');
const util = require('../utils');

module.exports = {
	name: 'guildCreate',
	run: async (client, guild) => {
		const total = guild.members.size;
		const users = guild.members.filter((m) => !m.user.bot).size;
		const bots = guild.members.filter((m) => m.user.bot).size;
		let Join = new MessageEmbed();
		Join.setTitle(`Joined ${guild.name}`)
			.setDescription(`${guild.id}`)
			.addField('Owner', `${guild.owner.user.tag || 'uncached'}`, true)
			.addField(`Members (${total})`, `${users} / ${bots}`, true)
			.addBlankField(true)
			.addField('Region', util.emotes.regions[guild.region] || guild.region, true)
			.addField(
				'Shard',
				`${client.options.shards[client.options.shards.length - 1] + 1}/${client.options.shards.length}`,
				true
			);
		const logs = client.channels.get(logChannel);
		//	const members = await guild.members.fetch();
		guild.utils = util;
		guild.guild = guild;

		if (logs) logs.send(Join);
		await util.db.setupServer(guild);
	}
};
