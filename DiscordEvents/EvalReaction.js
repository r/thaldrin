const p = require('phin').defaults({
	method: 'POST',
	parse: 'json'
});
const hastebin = require('hastebin-gen');
module.exports = {
	name: 'messageReactionAdd',
	run: async (client, reaction, user) => {
		if (user.bot) return;
		if (user.id !== '318044130796109825') return;
		if (reaction.emoji.name == '📥') {
			let haste;
			try {
				if (!client.lastEval) {
					await reaction.message.edit(`\`Unable to upload uncached eval results\``);
					await reaction.message.reactions.removeAll();
				} else {
					hastebin(`${client.lastEval || `Last eval resuts weren't cached`}`, {
						url: 'http://hasteb.in/documents',
						extension: 'js'
					})
						.then(async (h) => {
							await reaction.message.edit(`<${h}>`);
							await reaction.message.reactions.removeAll();
						})
						.catch((error) => {
							// Handle error
							console.error(error);
						});
				}
			} catch (err) {
				console.error(err);
			}
		}

		if (reaction.emoji.name == '🗑') {
			try {
				await reaction.message.delete();
			} catch (err) {}
		}
	}
};
