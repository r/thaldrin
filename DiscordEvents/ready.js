const {
	log,
	status
} = require('../utils');
const config = require('../config');
const blapi = require('blapi');
//blapi.setLogging(true);



module.exports = {
	name: 'ready',
	run: async (client) => {
		if (client.user.id === '434662676547764244') {
			blapi.handle(client, config.apis);
		}
		log.hasStarted();
		status.randomStatus(client)
		setInterval(() => {
			status.randomStatus(client);
		}, 60000);
	}
};