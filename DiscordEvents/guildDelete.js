const { logChannel } = require('../config');
const util = require('../utils');

module.exports = {
	name: 'guildDelete',
	run: async (client, guild) => {
		const logs = client.channels.get(logChannel);
		const members = await guild.members.fetch();
		guild.utils = util;
		guild.guild = guild;

		const total = guild.members.size;
		const users = guild.members.filter((m) => !m.user.bot).size;
		const bots = guild.members.filter((m) => m.user.bot).size;

		if (logs)
			logs.send(
				`Removed from ${guild.name} (owned by: ${guild.owner.user.tag ||
					'uncached'} ${guild.ownerID}) on shard ${client.options.shards[client.options.shards.length - 1] +
					1}/${client.options.shards.length}\nServer has ${parseInt(total).toLocaleString()} member${total > 1
					? 's'
					: ''}: ${parseInt(users).toLocaleString()} user${users > 1 ? 's' : ''}, ${parseInt(
					bots
				).toLocaleString()} bot${bots > 1 ? 's' : ''}, in region ${guild.region}`
			);
	}
};
