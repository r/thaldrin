const {
	Collection,
	MessageEmbed
} = require('discord.js');
const {
	table
} = require('quick.db');
const Servers = new table('servers');
const Users = new table('users');
const Backend = new table('backend');
const Trello = require('trello');
const config = require('../config');
const vars = require('../vars');
const {
	db,
	topic
} = require('../utils')
const trello = new Trello(config.trello.key, config.trello.token);
module.exports = {
	name: 'message',
	run: async (client, msg) => {
		if (msg.author.bot) return;
		//if (msg.author.id !== '318044130796109825') return;
		//console.log(msg.author.tag)

		let UserFromDB = await db.blacklist(msg.author.id, 's')
		if (UserFromDB.state) return;
		const DefaultPrefix = client.config.prefixes;
		const CustomPrefix = Servers.get(msg.guild.id);
		if (!CustomPrefix) {
			PrefixArray = [DefaultPrefix /* , CustomPrefix */ ].flat(Infinity);
		} else {
			PrefixArray = [DefaultPrefix, CustomPrefix.prefix].flat(Infinity);
		}
		let PREFIX;
		let EXISTS;
		for (p in PrefixArray) {
			if (msg.content.startsWith(PrefixArray[p])) {
				EXISTS = true;
				PREFIX = p;
			}
		}
		if (!EXISTS) return;
		//console.log(topic.includesSetting('thaldrin.no-cmds', msg.channel.topic))
		if (topic.includesSetting('thaldrin.no-cmd', msg.channel.topic)) return;

		const args = msg.content.slice(PrefixArray[PREFIX].length).trim().split(/ +/g);
		const command = args.shift().toLowerCase();
		const cmd = client.commands.find((c) => c.name == command || (c.aliases && c.aliases.includes(command)));


		const ctx = {
			send: msg.channel.send.bind(msg.channel),
			client,
			msg,
			args,
			command: cmd,
			me: msg.guild.me,
			guild: msg.guild,
			channel: msg.channel,
			author: msg.author,
			member: msg.member,
			trello,
			db: {
				users: Users,
				servers: Servers,
				backend: Backend
			},
			utils: require('../utils'),
			config: require('../config'),
			vars: require('../vars'),
			isDeveloper: vars.developers.find((dev) => msg.author.id == dev.id)
		};
		if (!cmd) return;

		if (!client.cooldowns.has(cmd.name)) {
			client.cooldowns.set(cmd.name, new Collection());
		}

		if (ctx.isDeveloper) {
			cmd.AuthorPermissions = 'NONE';
		}
		if (cmd.guildOnly && !msg.guild) return;
		if (cmd.nsfw && !ctx.channel.nsfw)
			return ctx.send('This channel is not set to NSFW, please mark it as such and rerun this command.');
		if (cmd.developerOnly && !vars.developers.find((dev) => msg.author.id == dev.id)) return;
		if (cmd.AuthorPermissions !== 'NONE' && !ctx.member.permissions.has(cmd.AuthorPermissions)) {
			return ctx.send(`You need the \`${cmd.AuthorPermissions}\` Permission(s) to run this Command`);
		}
		const now = Date.now();
		const timestamps = client.cooldowns.get(cmd.name);
		const cooldownAmount = (cmd.cooldown || 1) * 1000;

		if (timestamps.has(msg.author.id)) {
			const expirationTime = timestamps.get(msg.author.id) + cooldownAmount;

			if (now < expirationTime) {
				let CooldownEmb = new MessageEmbed();
				const timeLeft = (expirationTime - now) / 1000;
				CooldownEmb.setTitle(`${cmd.name} is on cooldown`).setDescription(
					`\`${cmd.name}\` has a cooldown of \`${cmd.cooldown} second${cmd.cooldown > 1
						? 's'
						: ''}\`\n Wait \`${`${Math.round(timeLeft)} second${Math.round(timeLeft) > 1
						? 's'
						: ''}`.replace('0 second', 'just a second longer')}\` before trying to use it again.`
				);
				return ctx.send(CooldownEmb);
			}
		} else {
			timestamps.set(msg.author.id, now);
			setTimeout(() => timestamps.delete(msg.author.id), cooldownAmount);

			cmd.command(ctx).then(async () => {
				if (!ctx.config.type.beta) ctx.client.channels.find(c => c.id === vars.logs.usage).send(`${ctx.utils.format.bold(ctx.author.tag)} (${ctx.utils.format.code(ctx.author.id)}) used ${ctx.utils.format.code(cmd.name)} in ${ctx.utils.format.bold(ctx.guild.name)} (${ctx.utils.format.code(ctx.guild.id)}) `).catch(e => console.log(e))
				await ctx.db.backend.add(`usage.${cmd.name}`, 1)
			}).catch((err) => {
				if (!cmd.name === 'e926' || !cmd.name === 'e621') {
					trello
						.addCard(
							cmd.name + ' | ' + err.message,
							`Full Error:
		 ${err}

		 Content: ${ctx.msg.content}
		 
		 Author: ${msg.author.tag} (${msg.author.id})
		 Server: ${msg.guild.name} (${msg.guild.id})`,
							config.trello.boards.errors
						)
						.then((r) => {
							trello
								.addLabelToCard(r.id, config.trello.labels.errors)
								.catch((error) => console.log(error));
						})
						.catch((err) => console.log(err));
				}
				//console.warn(err.message);
				let ErrorEmb = new MessageEmbed();
				ErrorEmb.setTitle(`${cmd.name} encountered an Error`)
					.setDescription(`\`${err.message}\`\n\n\`${err}\``)
					.setColor('RED');
				return ctx.send(ErrorEmb);
			});
		}
	}
};