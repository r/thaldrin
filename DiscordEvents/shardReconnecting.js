const {
    logChannel
} = require('../config');

module.exports = {
    name: 'shardReconnecting',
    run: async (client) => {
        const logs = client.channels.get(logChannel)
        const message = `Shard ${client.options.shards[client.options.shards.length - 1] + 1}/${client.options.shards.length} changed status to reconnecting`;

        if (logs) logs.send(message);
        console.log(message);
    }
}