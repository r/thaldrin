const {
  log,
  db
} = require("../utils/index");
const {
  SourceFynnder: {
    SourceFynnder
  },
  topic
} = require("../utils");
const {
  table
} = require("quick.db");
const Servers = new table("servers");
const Users = new table("users");
const Backend = new table("backend");


module.exports = {
  name: "message",
  run: async (client, Message) => {
    if (Message.author.bot) return;

    let UserFromDB = await db.blacklist(Message.author.id, 's')
    if (UserFromDB.state) return;

    if (topic.includesSetting('thaldrin.no-SF', Message.channel.topic)) return;

    let Server = Servers.get(Message.guild.id);
    let Enabled;
    if (Server === null) {
      Enabled = db.defaults.server.SourceFynnder;
    } else {
      Enabled = Server.SourceFynnder;
    }
    SourceFynnder(Enabled, Message);
  }
};