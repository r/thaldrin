const {
  eco,
  db
} = require("../utils");
const {
  table
} = require("quick.db");
const Servers = new table("servers");
const Users = new table("users");
const Backend = new table("backend");

module.exports = {
  name: "message",
  run: async (client, Message) => {
    if (Message.author.bot) return;
    if (!Message.guild) return;
    if (Message.author.id !== '318044130796109825') return;

    let UserFromDB = await db.blacklist(Message.author.id, 's')
    if (UserFromDB.state) return;

    await eco.CalculateFromMessage(Message)
    // console.log(Message)
  }
}