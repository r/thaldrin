const { logChannel } = require("../config");
const util = require("../utils");

module.exports = {
  name: "shardReady",
  run: async client => {
    const logs = client.channels.get(logChannel);
    const message = `Shard ${client.options.shards[
      client.options.shards.length - 1
    ] + 1}/${client.options.shards.length} is ready`;

    await util.log.shardReady(message);
    await util.log.shardSpinnerStarted(message);
    // if (logs !== undefined) logs.send(message);
  }
};
