const { logChannel } = require('../config');

module.exports = {
    name: 'shardResume',
    run: async (client, replayed) => {
        const logs = client.channels.get(logChannel);
        const message = `Shard ${client.options.shards[client.options.shards.length - 1] + 1}/${client.options.shards.length} changed status to resumed\nReported that ${replayed == 0 ? 'no events were' : `${replayed} event${replayed > 1 ? 's were' : ' was'}`} replayed to the client`;

        if (logs) logs.send(message);
        console.log(message);
    }
}